// linked_list_template.cpp: Singly Linked List (template)

#include <cstdlib>
#include <iostream>
#include <stdexcept>

const int NITEMS = 10; // small List

// List declaration ------------------------------------------------------------

template <typename T>
class List {
    protected:
        // struct Node
        struct Node {
            T   data;
            Node *next;
        };

        typedef Node * iterator;

        Node *head; // basically the only data member

    public:
        List() : head(nullptr) { } // if head is not set to nullptr,
        iterator front() { return head; }; // to publically access head.

        size_t size() const;
        T& at(const size_t i); // accessing like list.at(3);
        void insert(iterator it, const T &data); // insert after it
        void push_back(const T &data); // append
        void erase(iterator it); // erase
};

// List implementation --------------------------------------------------------

template <typename T>
size_t List<T>::size() const {
    size_t size = 0;

    for (Node *curr = head; curr != nullptr; curr = curr->next){
	size++;
    }

    return size;
}

template <typename T>
T& List<T>::at(const size_t i) {

    Node *curr = head;
    size_t num = 0;

    while(num < i && curr != nullptr){
        curr = curr->next;
        num++;
    }
    // reach the node theoretically indexed i

    if (curr != nullptr){
        return curr->data;
    } else {
        throw std::out_of_range("invalid list index");
    }
}

// Post-Condition: New Node is created with specified data value and placed
// after the iterator it.
template <typename T>
void List<T>::insert(iterator it, const T& data) {

    //TODO
}

// Post-Condition: New Node is create with specified data value and placed at
// the end of the list.
template <typename T>
void List<T>::push_back(const T& data) {

    // case 1: empty list
    if (head == nullptr) {
        head = new Node{data, nullptr};
        return;
    }

    // case 2:
    Node *curr = head;
    Node *tail = head;

    while (curr != nullptr){
        tail = curr;
        curr = curr->next;
    }
    // curr will point to nullptr, tail will point to the last node

    tail->next = new Node{data, nullptr};
}

template <typename T>
void List<T>::erase(iterator it) {
    if (it == nullptr) { // case 1
        throw std::out_of_range("invalid iterator");
    }

    if (head == it) { // case 2
        head = head->next;
        delete it;
    } else {    // case 3
        Node *node = head;

        while (node != nullptr && node->next != it) {
            node = node->next; // traverse one node
        }

        // node is now at it's previous node. (a)

        if (node == nullptr) { // it does not exist in this linked list.
            throw std::out_of_range("invalid iterator");
        }
                        //(b)
        node->next = it->next;
        delete it; // (c)
    }
}

// Main execution -------------------------------------------------------------

int main(int argc, char *argv[]) {
    List<int> list;

    std::cout << "List Size: " << list.size() << std::endl;

    for (int i = 0; i < NITEMS; i++) {
        list.push_back(i);
    }

    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    std::cout << "**** Insert" << std::endl;
    auto head = list.front();
    list.insert(head, NITEMS + 1);
    list.insert(head, NITEMS + 2);
    list.insert(head->next->next, NITEMS + 3);

    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    std::cout << "**** Erase" << std::endl;
    list.erase(list.front());
    list.erase(list.front()->next);
    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    return 0;
}
