// memoryleak.cpp: where is my mind

#include <cassert>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <utility>

const int NITEMS = 1<<10;
const int TRIALS = 100;

using namespace std;

template <typename T>
class Array {
public:
    typedef T* iterator;

    // Constructor
    Array(const int n = 0) : length(n), capacity(n ? n : 8), data(new T[capacity]) {}

    // Destructor
    ~Array() { delete [] data; }

    // Copy Constructor
    Array(const Array<T>& other) : length(other.length), capacity(other.capacity), data(new T[capacity]) {
	copy(other.data, other.data + length, data);
    }

    // Assignment Operator
    Array<T>& operator=(Array<T> other) {
    	swap(other);	// Copy and swap idiom
	return *this;
    }

    // Swap method
    void swap(Array<T>& other) {
	std::swap(length, other.length);
	std::swap(capacity, other.capacity);
	std::swap(data, other.data);
    }

    void push_back(const T &value) {
    	if (length >= capacity) {   // Check capcity and grow
    	    capacity = capacity * 4 / 3;
    	    T *tmp   = new T[capacity];
    	    copy(data, data + length, tmp);
    	    delete [] data;
    	    data     = tmp;
	}

    	data[length] = value;
	length++;
    }
    const size_t size() const	    { return length; }
    T& operator[](const int i)	    { return data[i]; }
    iterator begin()		    { return data; }
    iterator end()		    { return data + length; }

private:
    size_t  length;
    size_t  capacity;
    T*	    data;
};

bool duplicates(int n) {
    Array<int> randoms;

    for (int i = 0; i < NITEMS; i++) {
    	randoms.push_back(rand() % 1000);
    }

    for (int i = 0; i < n; i++) {
	if (find(randoms.begin(), randoms.end(), randoms[i]) != randoms.end()) {
	    return true;
	}
    }

    return false;
}

void array_test() {
    Array<int> a0;

    for (int i = 0; i < NITEMS; i++) {
    	a0.push_back(rand() % 1000);
    }

    Array<int> a1(a0);
    for (int i = 0; i < NITEMS; i++) {
    	assert(a0[i] == a1[i]);
    }

    Array<int> a2;
    a2 = a0;
    for (int i = 0; i < NITEMS; i++) {
    	assert(a0[i] == a2[i]);
    }
}

int main(int argc, char *argv[]) {
    srand(time(NULL));

    for (int i = 0; i < TRIALS; i++) {
    	if (duplicates(NITEMS)) {
	    cout << "Duplicates Detected!" << endl;
	}
    }

    array_test();

    return 0;
}
