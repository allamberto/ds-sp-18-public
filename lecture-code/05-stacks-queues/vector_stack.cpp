// vector_stack.cpp

#include "vector_stack.hpp"

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
    vector_stack<int> s;

    s.push(2);
    cout << s.top() << endl;
    s.push(3);
    cout << s.top() << endl;
    s.push(1);
    cout << s.top() << endl;
    s.pop();
    cout << s.top() << endl;
    s.pop();
    cout << s.top() << endl;

    return 0;
}
